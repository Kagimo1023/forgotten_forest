/*
* Copyright © 2020 Kagimo1023 <daniilxcxc2@gmail.com>
* Copyright © 2020 Ivan Petrov <iv4n.petrov@yandex.ru>
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 3 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef AUDIO_H
#define AUDIO_H

#include <threads.h>
#include <stdint.h>

_Bool submit_audio (void *const, size_t const length, uint_fast8_t const,
                    uint_fast32_t const);
thrd_t init_audio (void);

#endif
