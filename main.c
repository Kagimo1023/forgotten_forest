/*
* Copyright © 2020 Kagimo1023 <daniilxcxc2@gmail.com>
* Copyright © 2020 Ivan Petrov <iv4n.petrov@yandex.ru>
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 3 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <ncurses.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <locale.h>

#include "audio.h"
#include "effects.h"

#define MAP_WIDTH      36
#define MAP_HEIGHT     15

#define R_EMPTY        ' '
#define R_GRASS_A      '1'
#define R_GRASS_B      '2'
#define R_GRASS_C      '3'
#define R_WATER        '~'
#define R_MOUNTAIN     '^'
#define R_SPAWN        ';'
#define R_BUSH         '#'
#define R_FOOD         'X'
#define R_EXIT_LEVEL   '*'

#define EMPTY      L" "
#define GRASS_A    L"░"
#define GRASS_B    L"▒"
#define GRASS_C    L"▓"
#define WATER      L"~"
#define MOUNTAIN   L"^"
#define PLAYER     L"Ӄ"
#define SPAWN      L"†"
#define BUSH       L"#"
#define FOOD       L"๓"
#define EXIT_LEVEL L"*"

#define EMPTY_PAIR        1
#define WATER_PAIR        2
#define BUSH_PAIR         3
#define PLAYER_PAIR       4
#define FOOD_PAIR         5
#define MOUNTAIN_PAIR     6
#define GRASS_PAIR        7
#define EXIT_LEVEL_PAIR   8
#define SPAWN_PAIR        9

static struct cell
{
  cchar_t c;
  _Bool has_x;
  _Bool has_move;
} b_map[MAP_HEIGHT][MAP_WIDTH];

static void read_map (char *, struct cell (*)[MAP_HEIGHT][MAP_WIDTH]);
static void draw_map (WINDOW *, struct cell (*)[MAP_HEIGHT][MAP_WIDTH]);

static void check_X (struct cell (*)[MAP_HEIGHT][MAP_WIDTH], int y, int x);
static int is_move_okay (struct cell (*)[MAP_HEIGHT][MAP_WIDTH], int y,
                         int x);

static int kspawn_X, kspawn_Y;
static int kcor_Y, kcor_X;

static cchar_t xchar[50];

static int xspawn_X[50], xspawn_Y[50];
static int count_X, current_X;

static int epspawn_X, epspawn_Y;

int
main (void)
{
  setlocale (LC_ALL, "");

  int ch;

  if (!initscr ())
    {
      fprintf (stderr, "Error initialising ncurses.\n");
      exit (1);
    }

  init_audio ();
  initscr ();
  refresh ();

  load_effect (0, "Sounds/WALK_GA.wav");

  curs_set (0);

  keypad (stdscr, TRUE);
  cbreak ();
  noecho ();

  int offsetx = (COLS - MAP_WIDTH) / 2;
  int offsety = (LINES - MAP_HEIGHT) / 2;

  WINDOW *title = newwin (3, MAP_WIDTH, offsety - 3, offsetx);
  WINDOW *frame = newwin (MAP_HEIGHT, MAP_WIDTH, offsety, offsetx);
  WINDOW *wmap = derwin (frame, MAP_HEIGHT - 2, MAP_WIDTH - 2, 1, 1);
  WINDOW *tui = newwin (4, MAP_WIDTH, offsety + MAP_HEIGHT, offsetx);


  if (has_colors () == FALSE)
    {
      endwin ();
      printf ("Your terminal does not support color\n");
      exit (1);
    }

  start_color ();

  init_color (COLOR_WHITE, 700, 700, 700);
  init_color (COLOR_BLACK, 0, 0, 0);
  init_color (COLOR_GREEN, 0, 500, 0);
  init_color (COLOR_YELLOW, 500, 500, 0);
  init_color (COLOR_BLUE, 0, 0, 500);

  init_pair (EMPTY_PAIR, COLOR_WHITE, COLOR_BLACK);
  init_pair (SPAWN_PAIR, COLOR_GREEN, COLOR_BLACK);
  init_pair (GRASS_PAIR, COLOR_GREEN, COLOR_BLACK);
  init_pair (FOOD_PAIR, COLOR_BLACK, COLOR_YELLOW);
  init_pair (WATER_PAIR, COLOR_WHITE, COLOR_BLUE);
  init_pair (MOUNTAIN_PAIR, COLOR_WHITE, COLOR_BLACK);
  init_pair (BUSH_PAIR, COLOR_GREEN, COLOR_BLACK);
  init_pair (PLAYER_PAIR, COLOR_RED, COLOR_WHITE);
  init_pair (EXIT_LEVEL_PAIR, COLOR_RED, COLOR_BLACK);

  read_map ("Maps/LEVEL_1.txt", &b_map);

  touchwin (frame);
  
  wbkgd (frame, EMPTY_PAIR);
  wbkgd (title, EMPTY_PAIR);
  
  wcolor_set (frame, GRASS_PAIR, NULL);
  box (frame, 0, 0);

  wcolor_set (title, GRASS_PAIR, NULL);
  box (title, 0, 0);

  wcolor_set (tui, GRASS_PAIR, NULL);
  box (tui, 0, 0);

  wattron (title, COLOR_PAIR (EMPTY_PAIR));
  mvwaddstr (title, 1, 13, "Забытый Лес");
  wattroff (title, COLOR_PAIR (EMPTY_PAIR));

  wattron (tui, COLOR_PAIR (EMPTY_PAIR));
  mvwaddstr (tui, 2, 1,
             "  Для выхода из игры нажмите Q");
  wattroff (tui, COLOR_PAIR (EMPTY_PAIR));

  wrefresh (title);
  wrefresh (frame);

  kcor_Y = kspawn_Y;
  kcor_X = kspawn_X;

  WINDOW *curs = newwin (1, 1, kcor_Y, kcor_X);

  cchar_t cc;
  setcchar (&cc, PLAYER, A_BLINK, PLAYER_PAIR, 0);
  wadd_wch (curs, &cc);

  do
    {
      wmove (wmap, 0, 0);
      draw_map (wmap, &b_map);
      mvwin (curs, offsety + kcor_Y + 1, offsetx + kcor_X + 1);
      check_X (&b_map, kcor_Y, kcor_X);
      touchwin (wmap);
      touchwin (frame);
      mvwprintw (tui, 1, 1,
                 "  Количество собранных ๓ - %d/%d",
                 current_X, count_X);
      wrefresh (tui);
      wrefresh (wmap);
      wrefresh (curs);

      ch = getch ();

      switch (ch)
        {
        case KEY_UP:
          if (is_move_okay (&b_map, kcor_Y - 1, kcor_X))
            {
              play_effect (0);
              kcor_Y = kcor_Y - 1;
            }
          break;
        case KEY_DOWN:
          if (is_move_okay (&b_map, kcor_Y + 1, kcor_X))
            {
              play_effect (0);
              kcor_Y = kcor_Y + 1;
            }
          break;
        case KEY_LEFT:
          if (is_move_okay (&b_map, kcor_Y, kcor_X - 1))
            {
              play_effect (0);
              kcor_X = kcor_X - 1;
            }
          break;
        case KEY_RIGHT:
          if (is_move_okay (&b_map, kcor_Y, kcor_X + 1))
            {
              play_effect (0);
              kcor_X = kcor_X + 1;
            }
          break;
        }
    }
  while ((ch != 'q') && (ch != 'Q'));

  endwin ();

  exit (0);
}

static void
startup_logo (WINDOW * window)
{
}

static void
check_X (struct cell (*buffer)[MAP_HEIGHT][MAP_WIDTH], int y, int x)
{
  if ((*buffer)[y][x].has_x)
    {
      (*buffer)[y][x].has_x = 0;
      ++current_X;
    }
}

static int
is_move_okay (struct cell (*buffer)[MAP_HEIGHT][MAP_WIDTH], int y, int x)
{
  return (*buffer)[y][x].has_move;
}

static void
read_map (char *filename, struct cell (*buffer)[MAP_HEIGHT][MAP_WIDTH])
{
  register size_t x = 0, y = 0;

  FILE *file = fopen (filename, "r");

  char ch;

  while ((ch = getc (file)) != EOF)
    {
      cchar_t cc;

      switch (ch)
        {
        case R_SPAWN:
          kspawn_X = x;
          kspawn_Y = y;
          (*buffer)[y][x].has_move = 1;
          setcchar (&cc, SPAWN, A_NORMAL, SPAWN_PAIR, 0);
          break;
        case R_GRASS_A:
          (*buffer)[y][x].has_move = 1;
          setcchar (&cc, GRASS_A, A_NORMAL, GRASS_PAIR, 0);
          break;
        case R_GRASS_B:
          (*buffer)[y][x].has_move = 1;
          setcchar (&cc, GRASS_B, A_NORMAL, GRASS_PAIR, 0);
          break;
        case R_GRASS_C:
          (*buffer)[y][x].has_move = 1;
          setcchar (&cc, GRASS_C, A_NORMAL, GRASS_PAIR, 0);
          break;
        case R_BUSH:
          (*buffer)[y][x].has_move = 0;
          setcchar (&cc, BUSH, A_NORMAL, BUSH_PAIR, 0);
          break;
        case R_WATER:
          (*buffer)[y][x].has_move = 0;
          setcchar (&cc, WATER, A_NORMAL, WATER_PAIR, 0);
          break;
        case R_FOOD:
          (*buffer)[y][x - 1].has_move = 1;
          (*buffer)[y][x - 1].has_x = 1;
          ++count_X;
          continue;
        case R_MOUNTAIN:
          (*buffer)[y][x].has_move = 0;
          setcchar (&cc, MOUNTAIN, A_NORMAL, MOUNTAIN_PAIR, 0);
          break;
        case R_EXIT_LEVEL:
          (*buffer)[y][x].has_move = 1;
          setcchar (&cc, EXIT_LEVEL, A_BLINK, EXIT_LEVEL_PAIR, 0);
          break;
        case '\n':
          setcchar (&cc, L"\n", A_NORMAL, SPAWN_PAIR, 0);
          break;
          /* etc */
        default:
          /* unrecognized character, skip the iteration
             report an error in the future */
          continue;
        }

      (*buffer)[y][x++].c = cc;

      if (x >= MAP_WIDTH || ch == '\n')
        {
          x = 0;
          y++;
          if (y >= MAP_HEIGHT)
            return;
        }
    }
}

static void
draw_map (WINDOW * window, struct cell (*buffer)[MAP_HEIGHT][MAP_WIDTH])
{
  for (register ptrdiff_t y = 0; y < MAP_HEIGHT; y++)
    {
      for (register ptrdiff_t x = 0; x < MAP_WIDTH; x++)
        {
          register cchar_t const *const cc = &(*buffer)[y][x].c;

          if ((*buffer)[y][x].has_x)
            {
              cchar_t xcc;
              setcchar (&xcc, FOOD, A_NORMAL, FOOD_PAIR, 0);
              wadd_wch (window, &xcc);
            }
          else
            wadd_wch (window, cc);

          wchar_t wc[getcchar (cc, NULL, 0, 0, NULL)];
          getcchar (cc, wc, &(attr_t)
                    {
                    0}, &(short)
                    { 0 }, NULL);
          if (wc[0] == L'\n')
            break;
        }
    }
}
