/*
* Copyright © 2020 Kagimo1023 <daniilxcxc2@gmail.com>
* Copyright © 2020 Ivan Petrov <iv4n.petrov@yandex.ru>
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 3 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stddef.h>
#include <sndfile.h>
#include <ao/ao.h>

#include "audio.h"

static struct
{
  char *data;
  size_t length;
} playback_data;

static ao_device *device;

static mtx_t playing;
static cnd_t has_data;

static int
play_audio (register void *const arg)
{
  mtx_lock (&playing);
  do
    {
      cnd_wait (&has_data, &playing);
    }
  while (ao_play (device, playback_data.data, playback_data.length));
  return 0;
}

thrd_t
init_audio (void)
{
  ao_initialize ();

  mtx_init (&playing, mtx_plain);
  cnd_init (&has_data);

  thrd_t playback_thread;
  thrd_create (&playback_thread, play_audio, NULL);
  return playback_thread;
}

_Bool
submit_audio (register void *const buffer, register size_t const length,
              register uint_fast8_t const channels,
              register uint_fast32_t const sample_rate)
{
  static struct ao_sample_format current_format = {.byte_format =
      AO_FMT_NATIVE,.bits = 16
  };
  if (mtx_trylock (&playing) == thrd_success)
    {
      if (channels != current_format.channels
          || sample_rate != current_format.rate)
        {
          if (device)
            ao_close (device);
          current_format.channels = channels;
          current_format.rate = sample_rate;
          if (!
              (device =
               ao_open_live (ao_default_driver_id (), &current_format, NULL)))
            return 0;
        }
      playback_data.data = buffer;
      playback_data.length = length;
      mtx_unlock (&playing);
      cnd_broadcast (&has_data);
    }
  return 1;
}
