/*
* Copyright © 2020 Kagimo1023 <daniilxcxc2@gmail.com>
* Copyright © 2020 Ivan Petrov <iv4n.petrov@yandex.ru>
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 3 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <sndfile.h>
#include <ao/ao.h>
#include <threads.h>

#include "effects.h"
#include "audio.h"

#define MAX_EFFECTS    10

static struct
{
  short *data;
  size_t length;
  struct
  {
    uint_least8_t channels;
    uint_least32_t sample_rate;
  } pcm_info;
} effects[MAX_EFFECTS];

_Bool
load_effect (register ptrdiff_t const id, register char const *const filename)
{
  SF_INFO info;
  info.format = 0;
  register SNDFILE *file = sf_open (filename, SFM_READ, &info);
  if (!file)
    return 0;
  effects[id].pcm_info.sample_rate = info.samplerate;
  effects[id].pcm_info.channels = info.channels;
  effects[id].length = info.frames * info.channels * sizeof (short);
  sf_readf_short (file,
                  effects[id].data = realloc (effects[id].data,
                                              effects[id].length),
                  info.frames);
  sf_close (file);
  return 1;
}

_Bool
play_effect (register ptrdiff_t const id)
{
  return submit_audio (effects[id].data,
                       effects[id].length,
                       effects[id].pcm_info.channels,
                       effects[id].pcm_info.sample_rate);
}
