CFLAGS=-std=c17 -Wpedantic -Werror $$(pkg-config --cflags ncursesw ao sndfile)
LDFLAGS=-lpthread $$(pkg-config --libs ncursesw ao sndfile)
OBJECTS=main.o effects.o audio.o

quest: $(OBJECTS)
	$(CC) -o quest $(OBJECTS) $(LDFLAGS)
main.o effects.o audio.o: audio.h
main.o effects.o: effects.h
